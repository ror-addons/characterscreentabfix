------------------------------------------------------------------
----  Global Variables
------------------------------------------------------------------

CharacterScreenTabFix = {}


------------------------------------------------------------------
----  local Variables
------------------------------------------------------------------



------------------------------------------------------------------
----  Core Functions
------------------------------------------------------------------

function CharacterScreenTabFix.Initialize( )
	
	CharacterScreenTabFix.AttachEventAnchor()

end

function CharacterScreenTabFix.Shutdown( )

end


function CharacterScreenTabFix.AttachEventAnchor()
		CreateWindowFromTemplate( "CharacterWindowTabsFixInvisibleEventAnchorAttached", "CharacterWindowTabsFixInvisibleEventAnchor", "CharacterWindow" )
		WindowRegisterCoreEventHandler("CharacterWindowTabsFixInvisibleEventAnchorAttached", "OnShown","CharacterScreenTabFix.showTabs")
		WindowRegisterCoreEventHandler("ApothecaryWindowTitleBarText", "OnShown","CharacterScreenTabFix.makeApothecaryMovable")
		CharacterScreenTabFix.lockoutcall=true

end 

function CharacterScreenTabFix.showTabs()
    if CharacterScreenTabFix.lockoutcall then
		SendChatText(L".lock", L"")
		CharacterScreenTabFix.lockoutcall = false
	end
	WindowSetShowing("CharacterWindowTabs",true)
end

function CharacterScreenTabFix.makeApothecaryMovable()	
	WindowSetMovable("ApothecaryWindow",true)
end